package com.example.log4j2demo.com.example.log4j2demo;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@RequestMapping(value="/test",method = RequestMethod.POST)
	public void test(@Validated @RequestBody DateRange input){
		System.out.println(input.getStart());
		System.out.println(input.getEnd());
	}
}
